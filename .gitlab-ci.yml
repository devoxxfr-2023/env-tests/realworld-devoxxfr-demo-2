stages:
  - build-and-push
  - deploy
  - testing

include:
  - ci/configurations-variables.yml
  - ci/qovery-helpers.yml
  - ci/couchbase-helpers.yml
  - ci/cypress-helpers.yml
  - ci/hey-helpers.yml
  - ci/various-dependencies.yml

variables:
  !reference [.configuration, variables]

# -----------------------------
#  🐳 Web application
# -----------------------------
kaniko:build:push:web-app:
  stage: build-and-push
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    DOCKER_REGISTRY_IMAGE: $DOCKER_REGISTRY_USER/${APPLICATION_NAME}
  rules:
    # Production
    - if: $CI_COMMIT_BRANCH == "main"
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
    # Development
    - if: $CI_MERGE_REQUEST_IID
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
    # Release
    - if: $CI_COMMIT_TAG
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_TAG}
  script: |
    echo "${JSON_CONFIG}" > /kaniko/.docker/config.json
    cat /kaniko/.docker/config.json
    /kaniko/executor \
      --context $CI_PROJECT_DIR \
      --dockerfile Dockerfile \
      --destination $DOCKER_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG
    
    echo "🐳 Docker image (🌍 web app): ${DOCKER_REGISTRY_IMAGE}:${DOCKER_IMAGE_TAG}"

# -----------------------------
#  🚀 Web application
# -----------------------------
qovery:deploy:
  stage: deploy
  image: debian:buster-slim
  rules:
    # Production
    - if: $CI_COMMIT_BRANCH == "main"
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
        CURRENT_QOVERY_ENVIRONMENT: ${QOVERY_ENVIRONMENT}
    # Development
    - if: $CI_MERGE_REQUEST_IID
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
        CURRENT_QOVERY_ENVIRONMENT: ${QOVERY_ENVIRONMENT}-${CI_COMMIT_REF_NAME}
    # Release
    - if: $CI_COMMIT_TAG
      variables:
        DOCKER_IMAGE_TAG: ${CI_COMMIT_TAG}
        CURRENT_QOVERY_ENVIRONMENT: ${QOVERY_ENVIRONMENT}
  artifacts:
    paths:
      - deploy.env
    reports:
      dotenv: deploy.env
  environment:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}
    url: $DYNAMIC_ENVIRONMENT_URL
    on_stop: qovery:remove:environment
  before_script:
    - !reference [.install-qovery-cli, before_script]
    - !reference [.install-dnsutils, before_script]
  script:
    - !reference [.qovery_tools, script]
    - !reference [.clone-environment-if-not-exists-and-mr, script]
    - set_env DB_ENDPOINT ${DB_ENDPOINT}
    - set_env DB_HOST ${DB_HOST}
    - set_env DB_USERNAME ${DB_USERNAME}
    - set_env DB_PASSWORD ${DB_PASSWORD}
    - set_env DB_BUCKET ${DB_BUCKET}
    - set_env DB_SCOPE "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}"
    - DB_SCOPE="${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}"
    - echo "🌍 DB_SCOPE = ${DB_SCOPE}" # 🔎 check
    - echo "DB_SCOPE=${DB_SCOPE}" >> deploy.env # 💾 save the variable for the next job
    - !reference [.create-scope, script]
    - !reference [.deploy-container, script]
    - !reference [.update-environment-url, script]
    - set_env API_ROOT ${DYNAMIC_ENVIRONMENT_URL}
    - echo "API_ROOT=${DYNAMIC_ENVIRONMENT_URL}" >> deploy.env # 💾 save the variable for the next job

qovery:remove:environment:
  stage: deploy
  image: debian:buster-slim
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
  environment:
    name: ${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}
    action: stop
  allow_failure: true
  # Install the Qovery CLI
  before_script:
    - !reference [.install-qovery-cli, before_script]
    - !reference [.install-dnsutils, before_script]
  script:
    - !reference [.remove-environment, script]
    - !reference [.drop-scope, script]

# -----------------------------
#  🧪 Test application
# -----------------------------

.app:test:
  stage: testing
  image: gitpod/workspace-node
  rules:
    # Production
    #- if: $CI_COMMIT_BRANCH == "main"
    # Development
    - if: $CI_MERGE_REQUEST_IID
    # Release
    #- if: $CI_COMMIT_TAG
  artifacts:
    when: always
    reports:
      dotenv: deploy.env
    paths:
      - deploy.env
      - test-results.xml
      - cypress/videos/comments-spec.js.mp4
    reports:
      junit: test-results.xml
  needs:
    - job: qovery:deploy
      artifacts: true
  # Install Cypress and dep
  before_script:
    - !reference [.install-cypress, before_script]
  script:
    - cat deploy.env
    #- source deploy.env
    - !reference [.cypress-run, script]
    - echo "👋📝 content of deploy.env"
    - cat deploy.env
    #- echo "API_ROOT=${API_ROOT}" >> deploy.env # 💾 save the variable for the next job

.app:test:micro-benchmark:
  stage: testing
  image: gitpod/workspace-base
  rules:
    # Production
    #- if: $CI_COMMIT_BRANCH == "main"
    # Development
    - if: $CI_MERGE_REQUEST_IID
    # Release
    #- if: $CI_COMMIT_TAG
  artifacts:
    when: always
    reports:
      dotenv: deploy.env
    paths:
      - deploy.env
      - benchmark.txt
  needs:
    - job: app:test
      artifacts: true
  # Install Hey
  before_script:
    - !reference [.install-hey, before_script]
  script:
    - echo "👋📝 content of deploy.env"
    - cat deploy.env
    - !reference [.hey-run, script]


